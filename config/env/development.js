'use strict';

module.exports = {
  db: 'mongodb://' + (process.env.DB_PORT_27017_TCP_ADDR || 'localhost') + '/mean-dev',
  debug: true,
  logging: {
    format: 'tiny'
  },
  //  aggregate: 'whatever that is not false, because boolean false value turns aggregation off', //false
  aggregate: false,
  mongoose: {
    debug: false
  },
  hostname: 'http://localhost:3000',
  app: {
    name: 'Real Experts'
  },
  strategies: {
      local: {
        enabled: true
      },
      facebook: {
        clientID: 'DEFAULT_APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: 'http://localhost:3000/api/auth/facebook/callback',
        enabled: false
      },
      twitter: {
        clientID: 'NPMYb1JiRzgF63yYnIBxGoVKC',
        clientSecret: 'UJuFMPQtLdj0f66bFKZVAWFdJTSvGbsS4RJ62fhzH1uLYDBwf9',
        callbackURL: 'http://127.0.0.1:3000/api/auth/twitter/callback',
        enabled: true
      },
      github: {
        clientID: 'DEFAULT_APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: 'http://localhost:3000/api/auth/github/callback',
        enabled: false
      },
      google: {
        clientID: 'DEFAULT_APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: 'http://localhost:3000/api/auth/google/callback',
        enabled: false
      },
      linkedin: {
        clientID: '77lr54ychy4n0g',
        clientSecret: 'fjlCO3AtKdO7RzZS',
        callbackURL: 'http://127.0.0.1:3000/api/auth/linkedin/callback',
        enabled: true
      }
  },
  emailFrom: 'info@realexperts.co.uk',
  mailer: {
    service: 'SERVICE_PROVIDER', // Gmail, SMTP
    auth: {
      user: 'EMAIL_ID',
      pass: 'PASSWORD'
    }
  }, 
  secret: 'SOME_TOKEN_SECRET'
};
