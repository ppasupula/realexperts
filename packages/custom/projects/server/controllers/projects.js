'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Project = mongoose.model('Project'),
    _ = require('lodash');

module.exports = function(Projects) {

    return {
        /**
         * Find project by id
         */
        project: function(req, res, next, id) {
            Project.load(id, function(err, project) {
                if (err) return next(err);
                if (!project) return next(new Error('Failed to load project ' + id));
                req.project = project;
                next();
            });
        },
        /**
         * Create an project
         */
        create: function(req, res) {
            var project = new Project(req.body);
            project.user = req.user;

            project.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot save the project'
                    });
                }

                Projects.events.publish('create', {
                    description: req.user.name + ' created ' + req.body.title + ' project.'
                });

                res.json(project);
            });
        },
        /**
         * Update an project
         */
        update: function(req, res) {
            var project = req.project;

            project = _.extend(project, req.body);


            project.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot update the project'
                    });
                }

                Projects.events.publish('update', {
                    description: req.user.name + ' updated ' + req.body.title + ' project.'
                });

                res.json(project);
            });
        },
        /**
         * Delete an project
         */
        destroy: function(req, res) {
            var project = req.project;


            project.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the project'
                    });
                }

                Projects.events.publish('remove', {
                    description: req.user.name + ' deleted ' + project.title + ' project.'
                });

                res.json(project);
            });
        },
        /**
         * Show an project
         */
        show: function(req, res) {

            Projects.events.publish('view', {
                description: req.user.name + ' read ' + req.project.title + ' project.'
            });

            res.json(req.project);
        },
        /**
         * List of Projects
         */
        all: function(req, res) {
            var query = req.acl.query('Project');

            query.find({}).sort('-created').populate('user', 'name username').exec(function(err, projects) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the projects'
                    });
                }

                res.json(projects)
            });

        }
    };
}